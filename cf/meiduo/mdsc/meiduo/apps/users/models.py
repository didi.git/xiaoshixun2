from audioop import reverse
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


class User(AbstractUser):
    # Fields
    mobile = models.CharField(max_length=11)
    # Relationship Fields
    address = models.ForeignKey(
        'users.Address',
        on_delete=models.CASCADE, related_name="users",
    )

    class Meta:
        ordering = ('username',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('users_user_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('users_user_update', args=(self.pk,))


class Address(models.Model):
    # Fields
    title = models.CharField(max_length=255)
    receiver = models.CharField(max_length=50)
    province = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    district = models.CharField(max_length=30)
    detail_place = models.CharField(max_length=30)
    phone = models.CharField(max_length=11)

    # Relationship Fields
    user = models.ForeignKey(
        'users.User',
        on_delete=models.CASCADE, related_name="addresss",
    )

    # 声明表的意思
    class Meta:
        ordering = ('title',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('users_address_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('users_address_update', args=(self.pk,))