from django.conf.urls import url
from apps.users import views

app_label = 'users'

urlpatterns = [
    url('ls/', views.UserView.as_view())
]
